Typographic keyboard layout for Linux (XKB)
===========================================
**Author:** [Pavel Kretov](mailto:firegurafiku@gmail.com)  
**License:** [MIT License](http://en.wikipedia.org/wiki/MIT_License)

My variation of typographic layout for Linux, suitable for English, Spanish,
Russian and Ukrainian, with some historic characters. See project's wiki for
more information.
